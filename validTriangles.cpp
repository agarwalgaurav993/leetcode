#include <bits/stdc++.h>
using namespace std;

	bool check(int c, int t){
	    return c<t;
	}
	int b_search(int s, int e, int t, vector<int>& nums){
	    if(s == e){
	        if(check(nums[s], t)){
	            return s;
	        }
	        else
	            return -1;
	    }
	    int m = s + (e-s+1)/2;
	    if(check(nums[m], t))
	        return b_search(m, e, t, nums);
	    else
	        return b_search(s, m-1, t, nums);
	}
	int triangleNumber(vector<int>& nums) {
	    int cnt = 0;
	    sort(nums.begin(), nums.end());
	    int N = nums.size();
	    for(int i=0; i<N-2; i++){
	        for(int j=i+1; j<N - 1; j++){
	        	cout<<"For "<<nums[i]<<" "<<nums[j]<<", Index = ";
	            int index = b_search(j+1, nums.size()-1, nums[i]+nums[j], nums);
	            cout<<index<<endl;
	            if(index != -1){
	                cnt += index - j;
	            }
	        }
	    }
	    return cnt;
	}
int main(){
	int N;
	cin>>N;
	vector<int> arr(N);
	for(int i=0; i<N; i++){
		cin>>arr[i];
	}
	cout<<triangleNumber(arr)<<endl;
	return 0;
}