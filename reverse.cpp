#include <bits/stdc++.h>

using namespace std;

struct ListNode {
	  int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
 };

	ListNode* increment(ListNode *head, int links){
	    while(links > 1 && head->next){
	        links--;
	        head = head->next;
	    }
	    return head;
	}

	ListNode* reverse(ListNode* prev, ListNode* curr, ListNode* tail){
	    if(!curr || curr == tail->next)
	    	return tail;
	    ListNode *next = curr->next;
	    curr->next = prev;
	    return reverse(curr, next, tail);
	}
	ListNode* newNode(){
		ListNode* head = (ListNode*)malloc(sizeo(ListNode));
		return head;
	}
	ListNode* reverseBetween(ListNode* head, int m, int n) {
	     if(!head || n<= m)
	     	return head;
	     ListNode *t = newNode();
	     t->next = head;
	     ListNode *h1 = increment(t, m);
	     ListNode *h2 = increment(t, n);
	     if(h1 == h2){
	     	free(t);
	     	return head;
	     }
	     if(h2->next)
	     	h2 = h2->next;
	     /*Start is h1*/
	     ListNode *end = h2->next, *start = h1->next;
	     reverse(NULL, h1->next, h2);
	     h1->next = h2;
	     if(start)
	     	start->next = end;

	     return t->next;
	}