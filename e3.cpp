#include <iostream>
using namespace std;
unsigned reverse(unsigned num){
	unsigned int res = 0;
	for(int i =0; i<32; i++){
		if(num %2 == 0){
			/*Append 0*/
			res = res<<1 ;
		}
		else{
			/*Append 1*/
			res = (res<<1) + 1;
		}
		num = num>>1;
	}
	return res;
}

int main(){
	unsigned num;
	cin>>num;
	cout<<reverse(num)<<endl;
	return 0;
}