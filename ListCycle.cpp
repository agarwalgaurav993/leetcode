#include <bits/stdc++.h>
using namespace std;

struct ListNode {
	  int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
 };

 ListNode *ptr;
bool Cycle(ListNode *h1, ListNode* h2){
    if(!h1 || !h2)
        return false;
    if(h1 == h2){
        ptr = h1;
        return true;
    }
    
    h1 = h1->next;
    h2 = h2->next;
    if(h2)
        h2 = h2->next;
    return Cycle(h1, h2);
}
int cycleLength(ListNode* head){
    int len = 0;
    ListNode *t = head;
    do{
        len++;
        t = t->next;
    }while(t != head);
    return len;
}
ListNode *detectCycle(ListNode *head) {
    ptr = NULL;
    ListNode *t1 = head;
    ListNode *t2 = NULL;
    if(head)
        t2 = head->next;
    if(Cycle(t1, t2) == false)
        return NULL;
    int len = cycleLength(ptr);
    while(len){
        t1 = t1->next;
        len--;
    }
    while(head != t1->next){
        head = head->next;
        t1 = t1->next;
    }
    return head;
}

int main(){
	return 0;
}