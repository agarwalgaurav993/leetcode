#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
void reverse(char* s, int i, int j){
    while(i<j){
        char t = s[i];
        s[i] = s[j];
        s[j] = t;
        i++;
        j--;
    }
}
void reverseWords(char *s) {
    int IN = 0;
    int pos = -1;
    int i;
    for(i=0; s[i] != '\0'; i++){
        if(s[i] != ' ' && pos < 0){
            pos = i;
        }
        else if(s[i] == ' '){
            if(pos > -1)
            	reverse(s, pos, i-1);
            pos = -1;
        }
    }
    if(pos > -1){
    	reverse(s, pos, i-1);
    }
    IN = 0;
    int k=0;
    for(i=0; s[i] != '\0'; i++){
        if(s[i] != ' '){
            IN = 1;
            s[k++] = s[i];
        }
        else{
            if(IN){
                IN = 0;
                s[k++] = s[i];
            }
        }
    }
    s[k]  = '\0';
}

int main(){
	char *c = (char*)malloc(1000*sizeof(char));
	scanf("%[^\n]s", c);
	//printf("%s\n", c);
	/*for(int i=0; c[i] != '\0'; i++){
		printf("%c\n", c[i]);
	}*/
	reverseWords(c);
	printf("%s\n", c);
	free(c);
	return 0;
}