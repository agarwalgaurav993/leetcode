#include <bits/stdc++.h>

using namespace std;

	struct Node{
		int sum;
		int num1;
		int num2;
		
		Node(int x, int y) : sum(x+y), num1(x), num2(y) {}
	};

	bool operator<(const Node& a, const Node& b) {
		return a.sum < b.sum;
	}

	vector<pair<int, int> > pq_to_vector(priority_queue<Node> &pq){
		vector<pair<int, int> > v(pq.size());
		int i = pq.size()-1;
		while(!pq.empty()){
			Node n = pq.top();
			v[i--] = make_pair(n.num2, n.num1);
			pq.pop();
		}
		return v;
	}
	vector<pair<int, int> > kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k) {
		priority_queue<Node> pq;
		int i = 0;
		int j = 0;
		for(i=0; i<nums1.size() && k>0; i++){
			for(j=0; j<nums2.size() && k>0; j++){
				Node n(nums1[i], nums2[j]);
				pq.push(n);
				k--;
			}
		}

		for(; i<nums1.size() && !pq.empty(); i++){
			for(; j<nums2.size(); j++){
				Node n(nums1[i], nums2[j]);
				if(n < pq.top()){
					pq.pop();
					pq.push(n);
				}
				else{
					return pq_to_vector(pq);
				}
			}
		}

		return pq_to_vector(pq);
	}

int main(){
	int M, N;
	cin>>M>>N;
	vector<int> ar1(M);
	vector<int> ar2(N);

	for(int i=0; i<M; i++)
		cin>>ar1[i];
	for(int i=0; i<N; i++)
		cin>>ar2[i];
	int k;
	cin>>k;
	vector<pair<int, int> > v = kSmallestPairs(ar2, ar1, k);
	for(int i=0; i<v.size(); i++)
		cout<<v[i].first<<" "<<v[i].second<<endl;
	return 0;
}