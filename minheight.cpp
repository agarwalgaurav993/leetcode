#include <bits/stdc++.h>
using namespace std;
int myheight(int N, int me, vector<vector<int> >& tree, vector<int>& visited, int ar[]){
	visited[me] = 1;
	int height = 0;
	int pos = -1;
	for(int i=0; i<tree[me].size(); i++){
		if(!visited[tree[me][i]]){
			myheight(N, tree[me][i], tree, visited, ar);
			if(height < 1 + ar[tree[me][i]]){
				height = 1 + ar[tree[me][i]];
				pos = tree[me][i];
			}
		}
	}
	ar[me] = height;
	return pos;
}
void syncNeighbours(vector<vector<int> >& v, int j, int myheight, int tht[], vector<int>& visited){
	for(int i=0; i<v[j].size(); i++){
		if(visited[v[j][i]] == 1){

			visited[v[j][i]] = 0;
			tht[v[j][i]] = myheight + 1;
			syncNeighbours(v, v[j][i], tht[v[j][i]], tht, visited);
		}
	}
}
bool satisfy(int mx, int h, bool f){
	if(f)
		return mx == h || mx+1 == h;
	return mx == h;
}
vector<int> findMinHeightTrees(int N, vector<pair<int, int> >& edges) {
	vector<int> visited(N);
	vector<vector<int> > tree(N);
	vector<int> sol;
	int heights[N];
	for(int i=0; i<N-1; i++)
	{
		tree[edges[i].first].push_back(edges[i].second);
		tree[edges[i].second].push_back(edges[i].first);
	}
	int lst = myheight(N, 0, tree, visited, heights);
	if(lst != -1){
		visited[0] = 0;
		visited[lst] = 0;
		syncNeighbours(tree, 0, heights[0], heights, visited);
	}
	int mx = 0;
	bool f = false;
	for(int i=0; i<N; i++)
		mx = max(mx, heights[i]);
	if(mx&1)
		f = true;
	mx = mx>>1;
	for(int i=0; i<N; i++){
		if(satisfy(mx, heights[i], f))
			sol.push_back(i);
	}
	return sol;
}
int main(){
	int N;
	cin>>N;
	vector<pair<int, int> > tickets(N);
	for(int i=0; i<N; i++){
		cin>>tickets[i].first;
		cin>>tickets[i].second;
	}
	vector<int> v = findMinHeightTrees(N+1, tickets);
	for(int i=0; i<v.size(); i++){
		cout<<v[i]<<endl;
	}
	return 0;
}