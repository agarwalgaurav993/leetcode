#include <iostream>

using namespace std;

char getcode(int c){
    if(!c)
        return 'Z';
    else
        return 'A' + c - 1;
}
string convertToTitle(int n) {
    string res = "";
    while(n){
        res = getcode(n%26) + res;
        if(n%26 == 0)
            n = n/26 - 1;
        else
            n = n/26;
    }
    return res;
}

int main(){
	int num;
	cin>>num;
	cout<<convertToTitle(num)<<endl;
	return 0;
}