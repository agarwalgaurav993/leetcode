#include <bits/stdc++.h>
using namespace std;

	int nexti(int i, int N){
		i--;
		if(i<0)
			return N+i;
	}
	int nextj(int i, int N){
		i++;
		if(i >= N)
			return i - N;
	}
	int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
		int i = 0;
		int j = 0;
		int N = gas.size();
		bool flag = false;
		int balance = 0;
		while((!flag || i != j) && i<N && j<N){
			if(i == j)
				flag = true;
			if(balance < 0 && i!=j){
				i = nexti(i, N);
				balance += gas[i] - cost[i];
			}
			else{
				balance += gas[j] - cost[j];
				j = nextj(j, N);	
			}
		}
		if(!flag || balance < 0)
			return -1;
		else
			return i;
	}
int main(){
	int N;
	cin>>N;
	vector<int> gas(N);
	vector<int> cost(N);
	for(int i=0; i<N; i++)
		cin>>gas[i];
	for(int i=0; i<N; i++)
		cin>>cost[i];
	cout<<canCompleteCircuit(gas, cost)<<endl;
	return 0;
}