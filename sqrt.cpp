#include <iostream>
#define type int
using namespace std;
#define ll long long
    int condition_met(ll cval, ll key){
        	cval = cval*cval;
        	if(cval < 0 || cval > key)
        		return 1;
        	else
        		return -1;
        }

    int mySqrt(int size){
    	ll start = 0;
    	ll end = size;
    	ll mid;
    	while(end - start > 1 || end- start < 0){
    		mid = (start + end)>>1;
    		int status = condition_met(mid, size); 
    		/*Implies that mid on right side of required solution. This is Definately NOT what I require*/	
    		if(status == 1){
    			end = mid - 1;
    		}
    		/*Implies that mid is on left side of required solution. This might be what I require*/
    		else{
    			start = mid;
    		}
    	}
    	if(start == end){
    			return (int)start;
                cout<<"Start == END"<<endl;
    	}
    	else{
    		int SFLAG = condition_met(start, size);
    		int EFLAG = condition_met(end, size);
    		if(EFLAG == -1){
                cout<<"EFLAG == -1"<<start<<endl;
    			return (int)end;
            }
    		if(SFLAG == -1)
    			return (int)start;
    	}
    	return -1;
    }

int main(){
    int size;
    cin>>size;
    cout<<"\n\nResult : "<<mySqrt(size)<<endl;
    return 0;
}