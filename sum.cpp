#include <bits/stdc++.h>
using namespace std;

vector<vector <int> > n;
void NumArray(vector<int> nums) {
    n.resize(nums.size());
    for(int i=0; i<nums.size(); i++){
        vector<int> t;
        t.push_back(nums[i]);
        int k=0;
        for(int j=i+1; j<nums.size(); j++){
            t.push_back(t[k] + nums[j]);
            k++;
        }
        n[i] = t;
    }
    for(int i=0; i<n.size(); i++){
    	for(int j=0; j<n[i].size(); j++){
    		cout<<n[i][j]<<" ";
    	}
    	cout<<endl;
    }
}

int sumRange(int i, int j) {
        
    int sum = 0;
    if(j> i || i >= n.size())
        return sum;
    if(j >= n.size())
        j = n.size()-1;
    cout<<i<<" "<<j-i;
    return n[i][j-i];
}

int main(){
	int size;
	cin>>size;
	vector<int> ar;
	for(int i=0; i<size; i++){
		int t;
		cin>>t;
		ar.push_back(t);
	}
	NumArray(ar);
	cout<<sumRange(0,2);
	return 0;
}