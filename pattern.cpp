#include <bits/stdc++.h>
using namespace std;

/*bool check_update(string s, char c,cell mapp[]){
        if(mapp[c - 'a'].str == ""){
            mapp[c - 'a'].str = s;
            return true;
        }
        else if(mapp[c - 'a'].str == s){
            return true;
        }
        else{
            return false;
        }
    }
    */
bool wordPattern(string pattern, string str) {
	map<string, string> backmap;
	map<string, string> forwardmap;
    istringstream iss(str);
    int i = 0;
    while(iss && i < pattern.length()){
        string s;
        iss>>s;
        string k;
        k.push_back(pattern[i]);
        cout<<"Current String "<<s<<endl;
        cout<<"Current Character "<<k<<endl;
        if(s!= "" && backmap[s] == "" && forwardmap[k] == ""){
        	/*Insert the mapping*/
        	backmap[s] = k;
        	forwardmap[k] = s;
        }
        else if(s != "" && backmap[s] == k && forwardmap[k] == s){
        	/*Continue*/
        }
        else{
        	return false;
        }
        i++;
    }
    string s;
    iss>>s;
    if(s!= "" || i != pattern.size()){
        return false;
    }
    return true;
}

int main(){
	string pattern;
	string str;
	cin>>str;
	cin>>pattern;
	cout<<wordPattern("abcd", "")<<endl;
	return 0;
}