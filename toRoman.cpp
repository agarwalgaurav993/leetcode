#include <bits/stdc++.h>

using namespace std;

string getSymbol(int n){
    switch(n){
        case 1: return "I";
        case 5: return "V";
        case 10:    return "X";
        case 50:    return "L";
        case 100:   return "C";
        case 500:   return "D";
        case 1000:  return "M";
    }
}

string backString(string main, string pre){
	if(main == "IIII"){
		if(pre == "V"){
			return "IX";
		}
		else
			return "IV";
	}
	if(main == "XXXX"){
		if(pre == "L"){
			return "XC";
		}
		else
			return "XL";
	}
	if(main = "CCCC"){
		if(pre == "D"){
			return "CM";
		}
		else
			return "CD";
	}
}
string intToRoman(int num) {
    int ar[] = {1000, 500, 100, 50, 10, 5, 1};
    string forward = "";
    int ptr = 0;
    while(num && ptr < 7){
        while(num >= ar[ptr]){
            forward = forward + getSymbol(ar[ptr]);
            num = num - ar[ptr];
        }
        ptr++;
    }

    /*Backward compress*/
    
    return forward;	

}

int main(){
	int N;
	cin>>N;
	cout<<intToRoman(N)<<endl;
	return 0;
}