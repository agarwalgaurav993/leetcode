#include <bits/stdc++.h>
using namespace std;

void integerReplacement(int n) {
    if(n < 3)
        return;
    int N = (n&1)?n+1:n;
    int ar[N+1];
    ar[1] = 0;
    ar[2] = 1;
    for(int i=3; i<=N; i+=2){
        ar[i+1] = 1 + ar[((i+1)>>1)];
        ar[i] = 1+ min(ar[i-1], ar[i+1]);
        cout<<ar[i]<<endl;
        cout<<ar[i+1]<<endl;
    }
    return;
}
int main()
{
	integerReplacement(40);
	return 0;
}