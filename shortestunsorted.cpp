#include <bits/stdc++.h>

using namespace std;

	bool condition(int curr, int key){
		if(curr > key)
			return false;
		else
			return true;
	}
	int findPos(vector<int>& arr,int begin, int end, int key){
		/*T, T, T, T, T, F, F, F*/
		/*Find the last T*/
		if(begin == end){
			if(condition(arr[begin], key)){
				return begin;
			}
			else
				return begin - 1;
		}
		else if(begin < end){
			int mid = (begin + end) / 2;
			if(condition(arr[mid], key)){
				if(condition(arr[mid+1], key))
					return findPos(arr, mid+1, end, key);
				else
					return mid;
			}
			else{

				return findPos(arr, begin, mid -1, key);
			}
		}
		else if(begin > end){

			return findPos(arr, begin, begin, key);
		}
	}
	int findUnsortedSubarray(vector<int>& nums) {
		/*Find first maximal sorted array*/
		int i = 1;
		for(i = 1; i < nums.size(); i++){
			if(nums[i] < nums[i-1])
				break;
		}
		int start = 0;
		int end = i - 1;
		int max = nums[end];

		int low = i;
		int high = i;
		for(; i< nums.size(); i++){
			int pos;
			if(nums[i] < max){
				if(low != -1){
					pos = findPos(nums, start, end, nums[i]);
					cout<<nums[i]<<" has pos : "<<pos<<endl;
					low = min(low, pos);
				}
				high = i;
			}
			else{
				max = nums[i];
			}
		}
		return high - low;

	}


int main()
{
	int size;
	cin>>size;
	vector<int>  arr(size);
	for(int i=0; i < arr.size(); i++){
		cin>>arr[i];
	}
	cout<<findUnsortedSubarray(arr)<<endl;
}