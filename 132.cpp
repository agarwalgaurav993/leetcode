#include <bits/stdc++.h>
using namespace std;
bool satisfy(int a, int c, int b){
	return a< c && c < b;
}
bool find132pattern(vector<int>& nums) {
    if(nums.size() < 3)
        return false;
    int i, k;
    int j = 0;
	bool f = false;
	while(!f && j<nums.size()){
		i = j;
		j = i+1;
		k = j+1;
		if(nums[i] < nums[j]){
			while(!f && k<nums.size()){
				f = satisfy(nums[i], nums[k], nums[j]);
				if(nums[k] >= nums[j]){
					while(j<k){
						if(nums[j] < nums[i])
							i = j;
						j++;
					}
				}
				k++;
			}
		}

	}
	return f;
}
int main(){
	int N;
	cin>>N;
	vector<int> v(N);
	for(int i=0; i<N; i++)
		cin>>v[i];
	cout<<find132pattern(v)<<endl;
	return 0;
}