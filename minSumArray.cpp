#include <bits/stdc++.h>
using namespace std;

int push(vector<int>& nums, int& i){
	int size = nums.size();
    if(i < size - 1){
        ++i;
        return nums[i];
    }
    return 0;
}
int pop(vector<int>& nums, int &i, int& j){
    if(i<=j)
        return nums[i++];
    return 0;
}
int minSubArrayLen(int s, vector<int>& nums) {
    int i=-1, j=0;
    int sum = 0;
    int num = 1;
    int m = INT_MAX;
    while((num = push(nums, i)) != 0){
        sum += num;
        while(sum > s && (num = pop(nums, j, i)) != 0){
            sum -= num;
        }
        if(sum == s){
            m = min((i - j + 1), m);
        }
        
    }
    return m;
}
int main(){
	int s;
	int N;
	cin>>N;
	vector<int> ar(N);
	for(int i=0; i<N; i++){
		cin>>ar[i];
	}
	cin>>s;
	cout<<minSubArrayLen(s, ar)<<endl;
	return 0;
}