#include <bits/stdc++.h>

using namespace std;

bool check(int curr, int target){
	if(curr > target)
		return -1;
	if(curr == target)
		return 0;
	else
		return 1;
}

int bsearch(int s, int e, int t, vector<int> & nums){
	if(s > e)
		return -1;
    if(s == e){
    	if(check(nums[s], t)){
    		return s;
    	}
    	else
    		return -1;
    }
    int m = (s+e)/2;
    if(check(nums[m], t))
        return bsearch(s, m, t, nums);
    else
        return bsearch(m+1, e, t, nums);
}
int getRotationPos(vector<int>& nums){
	int s = 0;
	int e = nums.size() - 1;
	int t = nums[e];
	int pos = bsearch(s, e-1, t, nums);
	if(pos == -1)
		pos = e;
	return pos;
}
bool check1(int curr, int target){
	return curr < target;
}

int bsearch1(int s, int e, int t, vector<int> & nums){
	if(s > e)
		return -1;
    if(s == e){
    	if(nums[s] == t){
    		return s;
    	}
    	else
    		return -1;
    }
    int m = (s+e)/2;
    if(check1(nums[m], t))
        return bsearch1(m+1, e, t, nums);
    else
        return bsearch1(s, m, t, nums);
}
bool search(vector<int>& nums, int target) {
    if(nums.empty())
    	return false;
    int pos = getRotationPos(nums);
    cout<<pos<<endl;
    return bsearch1(0, pos-1, target, nums) != -1 || bsearch1(pos, nums.size()-1, target, nums) != -1;
}

int main(){
	int N, target;
	cin>>N;

	vector<int> ar(N);
	for(int i=0; i<N; i++)
		cin>>ar[i];
	cin>>target;
	cout<<search(ar, target)<<endl;
	return 0;
}