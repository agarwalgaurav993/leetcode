#include <bits/stdc++.h>

using namespace std;

	bool validchar(char c){
	    if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
	        return true;
	    return false;
	}
	char getlower(char c){
	    if(c >= 'A' && c <= 'Z')
	        return c + 32 ;
	    return c;
	}
	bool check(string s, string::iterator f, string::iterator b){
	    if(f == b)
	        return true;
	    else if(f - b > 1)
	        return true;
	    else{
	        if(validchar(*f) && validchar(*b)){
	        	cout<<getlower(*f)<<" "<<getlower(*b)<<endl;
	            if(getlower(*f) == getlower(*b))
	                return check(s, ++f, --b);
	            else
	                return false;
	        }
	        else{
	            if(!validchar(*f))
	                f++;
	            if(!validchar(*b))
	                b--;
	            return check(s, f, b);
	        }
	    }
	}
	bool isPalindrome(string s) {
		int f = 0;
		int e = s.length() -1;
		while(f<e){
			if(!validchar(s[f])){
				f++;
			}
			else if(!validchar(s[e])){
				e--;
			}
			else if(getlower(s[f]) != getlower(s[e])){
				return false;
			}
			else{
				f++;
				e--;
			}
		}
	    return true;
	}

int main(){
	cout<<isPalindrome("AP\nPA\n\n\naba\nAPPa")<<endl;
	return 0;
}