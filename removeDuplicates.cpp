#include <bits/stdc++.h>

using namespace std;

bool check(int c, int t){
    return c <= t;
}
int bsearch(int s , int e, int t, vector<int> nums){
    if(s == e){
        if(check(nums[s], t)){
            return s;
        }
        else
            return -1;
    }
    int m = s + (e-s+1)/2;
    if(check(nums[m], t))
        return bsearch(m, e, t, nums);
    else
        return bsearch(s, m-1, t, nums);
}
int removeDuplicates(vector<int>& nums) {
    int k = 0;
    for(int i=0; i<nums.size();){
        nums[k++] = nums[i];
        int j = bsearch(i, nums.size()-1, nums[i], nums);
        cout<<"For Number "<<nums[i]<<"\nStart "<< i<<"\tEnd "<<j<<endl;
        if(i!= j && j!= -1){
            nums[k++] = nums[j];
            i = j + 1;
        }
        else
            i++;
    }
    return k;
}
void print(vector<int> &arr, int N){
	for(int i=0; i<N; i++){
		cout<<arr[i]<<" ";
	}
	cout<<endl;
}
int main(){
	int N;
	cin>>N;
	vector<int> arr(N);
	for(int i=0; i<N; i++){
		cin>>arr[i];
	}
	print(arr, removeDuplicates(arr));
	return 0;
}