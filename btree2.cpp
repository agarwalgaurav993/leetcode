#include <bits/stdc++.h>

using namespace std;

	int position(int val, vector<int>& v, int i, int j){
		for(; i<= j; i++)
			if(v[i] == val)
				return i;
		return -1;
	}

	TreeNode* buildTree(vector<int>& preorder, int& ptr, vector<int> inorder, int i, int j){
		if(ptr < 0 || i>j || j >= inorder.size())
			return NULL;
		TreeNode* root = (TreeNode* )malloc(sizeof(TreeNode));
		root->val = preorder[ptr];
		ptr--;
		int pos = position(root->val, inorder, i, j);
		root->right = buildTree(preorder, ptr, inorder, pos+1, j);
		root->left = buildTree(preorder, ptr, inorder, i, pos-1);
		return root;
	}

	TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
		int k = preorder.size();
		return buildTree(preorder, k-1, inorder, 0, k-1);	
	}