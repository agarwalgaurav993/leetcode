#include <iostream>
#define type int
using namespace std;
/*To Find floor in sorted array*/
int condition_met(type cval, type key){
	cval = cval*cval;
	if(cval < 0 || cval > key)
		return 1;
	else
		return -1;
}

int binary_search(type arr[], int size, type key){
	int start = 0;
	int end = size - 1;
	int mid;
	while(end - start > 1){
		mid = (start + end)>>1;
		int status = condition_met(arr[mid], key); 
		/*Implies that mid on right side of required solution. This is Definately NOT what I require*/	
		if(status == 1){
			end = mid - 1;
		}
		/*Implies that mid is on left side of required solution. This might be what I require*/
		else{
			start = mid;
		}
	}
	if(start == end){
		if(condition_met(arr[start], key) == -1)
			return start;
	}
	else{
		int SFLAG = condition_met(arr[start], key);
		int EFLAG = condition_met(arr[end], key);
		if(EFLAG == -1)
			return end;
		if(SFLAG == -1)
			return start;
	}
	return -1;
}
int main(){
	int size;
	int ar[100];
	cin>>size;
	for(int i = 0; i<size; i++){
		cin>>ar[i];
	}
	int key;
	cout<<"Enter the Key whose floor is to be found:";
	cin>>key;
	int state = binary_search(ar, size, key);
	if(state == -1){
		cout<<"Lower limit does not exists"<<endl;
	}
	else{
		cout<<"Lower Limit : "<<ar[state]<<endl;
	}
	return 0;
}