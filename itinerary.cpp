#include <bits/stdc++.h>

using namespace std;
void iterate(vector<string>& v, map<string, vector<string> >& mp, string  src){

}
bool getValid(vector<int>& v, vector<vector<int> > &odeg, int t, int s){
	v.push_back(s);
	if(v.size() == t)
		return true;
	for(int i=0; i<odeg[s].size(); i++){
		if(odeg[s][i]){
			odeg[s][i]--;
			if(getValid(v, odeg, t, i))
				return true;
			odeg[s][i]++;
		}
	}
	v.pop_back();
	return false;
}
vector<string> findItinerary(vector<pair<string, string> > tickets) {
	map<string, int> mp;
	map<int, string> rmp; 
	for(int i=0; i<tickets.size(); i++){
		mp[tickets[i].first] = 0;
		mp[tickets[i].second] = 0;
	}
	map<string, int> :: iterator it;
	int i = 0;
	for(it = mp.begin(); it != mp.end(); it++){
		rmp[i] = it->first;
		mp[it->first] = i++;
	}
	vector<vector<int> > v(mp.size(), vector<int>(mp.size()));
	for(int i=0; i<tickets.size(); i++){
		v[mp[tickets[i].first]][mp[tickets[i].second]]++;
	}
	//print(v);
	vector<int> r;
	getValid(r, v, tickets.size()+1, mp["JFK"]);
	vector<string> rs;
	for(int i=0; i<r.size(); i++)
		rs.push_back(rmp[r[i]]);
	return rs;
}
int main(){
	int N;
	cin>>N;
	vector<pair<string, string> > tickets(N);
	for(int i=0; i<N; i++){
		cin>>tickets[i].first;
		cin>>tickets[i].second;
	}
	vector<string> v = findItinerary(tickets);
	for(int i=0; i<v.size(); i++){
		cout<<v[i]<<endl;
	}
	return 0;
}