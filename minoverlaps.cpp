#include <bits/stdc++.h>
using namespace std;
	static bool cmp(Interval a, Interval b){
		if(a.start > b.start)
			return false;
		if(a.start < b.start)
			return true;
		if(a.end <= b.end)
			return true;
		else
			return false;
	}
	int eraseOverlapIntervals(vector<Interval>& intervals) {
		sort(intervals.begin(), intervals.end(), cmp);
		int end = INT_MIN;
		int overlaps = 0;
		int i=0;
		while(i < intervals.size()){
			if(end <= intervals[i].start){
				end = intervals[i].end;
				
			}
			else{
				/*Skip The interval*/
				overlaps++;	
			}
			i++;
		}
		return overlaps;
	}