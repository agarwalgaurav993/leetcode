#include <bits/stdc++.h>

using namespace std;

bool detectCycle(int i, vector<vector<int> >& v, vector<int>& visited){
	if(visited[i] == 1)
		return false;
	visited[i] = 1;
	for(int j=0; i<v[i].size(); j++){
		if(detectCycle(v[i][j], v, visited) == false)
			return false;
	}
	visited[i] = 2;
	return true;
}
bool canFinish(int numCourses, vector<pair<int, int>>& prerequisites) {
    vector<vector<int> > v(numCourses);
    for(int i=0; i<prerequisites.size(); i++){
        v[prerequisites[i].second] = prerequisites[i].first;
    }
    vector<int> visited[numCourses];
    for(int i=0; i<numCourses; i++)
    	if(visited[i] == 0)
    		if(detectCycle(i, v, visited))
    			return false;
    return true;
}