#include <bits/stdc++.h>

using namespace std;
	int getAr(int i, int j, int m, int n, vector<vector<int> > ar){
		if(i<0 || j<0 || i>=m || j>= n){
			return 0;
		}
		return ar[i][j];
	}

	bool dp(vector<int>& nums, int m ,int n){
		vector<vector<int> > v(m, vector<int>(n));
		for(int i=0; i< m; i++){
			ar[i][0] = 1;
		}
		for(int i=1; i<m; i++){
			for(int j=1; j<n; j++){
				ar[i][j] = getAr(i-1, j, m, n, ar) | getAr(i-1, j - nums[i-1], m, n, ar) ;
			}
			if(ar[i][n-1]){
				return true;
			}
		}
		return false;
	}

	bool canPartition(vector<int>& nums) {
		int sum = 0;
		for(int i=0; i<nums.size(); i++){
			sum += nums[i];
		}
		if(sum&1){
			return false;
		}
		int cols = (sum>>1) + 1;
		int rows = nums.size() + 1;
		return dp(nums, nums.size() + 1, (sum>>1) + 1);
		
	}