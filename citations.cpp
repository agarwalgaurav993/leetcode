#include <bits/stdc++.h>

using namespace std;

static bool check(int a , int b){
	return a > b;
}
int hIndex(vector<int>& citations) {
	sort(citations.begin(), citations.end(), check);
	int i=0;
	for(i=0; i<citations.size(); i++){
		if(citations[i] < i)
			break;
	}
	if(i == citations.size())
		return min(citations.size(), citations[i-1]);
	else
		return i-1;
}