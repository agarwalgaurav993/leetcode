#include <iostream>
#include <malloc.h>
#include <bits/stdc++.h>
using namespace std;
int* preSuf(string s){
    int *arr = (int*)malloc(sizeof(int)*s.length());
    memset(arr, 0, sizeof(int)*s.length());
    int j = 0;
    int i = j + 1;
    for(; i< s.length(); i++){
        while(j != 0 && s[i] != s[j]){
                j = arr[j-1];
            }
        if(s[i] == s[j]){
            arr[i] = j + 1;
            j++;
        }
        else{
            arr[i] = j;
        }
    }
    return arr;
}
int strStr(string haystack, string needle){
	int *parr = preSuf(needle);
	int hptr = 0;
	int nptr = 0;
	for(int i=0; i<needle.size(); i++)
		cout<<parr[i]<<" ";
	cout<<endl;
	for(; hptr < haystack.length() && nptr < needle.length(); hptr++){
		while(nptr && haystack[hptr] != needle[nptr]){
			nptr = parr[nptr - 1];
		}
		if(haystack[hptr] == needle[nptr]){
			nptr++;
		}
	}
	free(parr);
	if(nptr == needle.length())
		return hptr - needle.length();
	return -1;
}

int main(){
	string h, n;
	cin>>h;
	cin>>n;
	cout<<strStr(h, "")<<endl;
	return 0;
}