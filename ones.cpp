#include <bits/stdc++.h>

using namespace std;

	vector<int> getCnt(string s){
	    vector<int> res;
	    int z = 0;
	    int o = 0;
	    for(int i=0; i<s.length(); i++){
	        if(s[i] == '0')
	            z++;
	        else
	            o++;
	    }
	        res.push_back(z);
	        res.push_back(o);
	    return res;
	}
	int **getNewArray(int m, int n){
	    int **ar = (int**) malloc(m*sizeof(int*));
	    for(int i = 0; i < m; i++){
	        ar[i] = (int*)malloc(n*sizeof(int));
	    }
	    return ar;
	}
	void freeMem(int **ar, int m){
		for(int i=0; i<m; i++){
			free(ar[i]);
		}
	}
	int getAr(int** ar, int i, int j){
		if(i<0 || j<0)
			return 0;
		return ar[i][j];
	}

	void print(int** ar, int m, int n){
		for(int i=0; i<m; i++){
	        for(int j=0; j<n; j++)
	        	cout<<ar[i][j]<<" ";
	        cout<<endl;
		}
	}
	int getVal(int k, vector<string>& strs, int** base, int m, int n, int arr[][2]){
	    
	    if(k == strs.size()){
	    	int r  = base[m-1][n-1];
	    	freeMem(base, m);
	        return r;
	    }
	    int **nbase = getNewArray(m, n);
	    //cout<<"IN getVal\n";
	    for(int i=0; i<m; i++)
	        for(int j=0; j<n; j++){
	            nbase[i][j] = base[i][j];
	            if(arr[k][0] <= i && arr[k][1] <=j){
	            	//cout<<"MY PREB\n";
	                nbase[i][j] = max(nbase[i][j], 1 + getAr(base,i - arr[k][0],j - arr[k][1]));
	            }
	        }
	    freeMem(base, m);
	    return getVal(k+1, strs, nbase, m, n, arr);
	}
int findMaxForm(vector<string>& strs, int m, int n) {
    int ar[strs.size()][2];
    for(int i=0; i<strs.size(); i++){
        vector<int> t = getCnt(strs[i]);
        ar[i][0] = t[0];
        ar[i][1] = t[1];
    }
    cout<<"IN findMax\n";
    int **base = getNewArray(m+1, n+1);
    for(int i = 0; i<=m; i++){
        memset(base[i], 0, (n+1)*sizeof(int));
    }
    return getVal(0, strs, base , m+1, n+1, ar);
}

int main(){
	
	int n;
	int M, N;
	cin>>n;
	cin>>M>>N;
	vector<string> v(n);
	for(int i=0; i<n; i++)
		cin>>v[i];
	cout<<M<<N;
	cout<<findMaxForm(v, M, N);
	return 0;
}