#include <bits/stdc++.h>

using namespace std;
	int getMask(int pos){
		int mask = 1 << (pos - 1);
		return mask;
	}
	int getBit(int x, int pos){
		int mask = getMask(pos);
		return (x & mask) == mask;
	}
	int rangeBitwiseAnd(int m, int n) {
		int num = 0;
		bool flag = false;
		for(int i=32; i>=1; i--){
			if((getBit(m, i) ^ getBit(n, i)) == 1)
				break;
			if(flag && (getBit(m, i) == 0 || getBit(n, i) == 0))
				break;
			if(getBit(m,i) && getBit(n,i))
				num = num | getMask(i);
		}
		
		return num;
	}
	int main()
	{
		int x, y;
		cin>>x>>y;
		cout<<rangeBitwiseAnd(x, y)<<endl;

		return 0;
	}