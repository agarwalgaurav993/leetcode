#include <bits/stdc++.h>
using namespace std;
	vector<vector<int> > v;

	int countMe(int num, vector<int>& nums, int &i){
		int cnt = 0;
		while(i<nums.size() && nums[i] == num){
			cnt++;
			i++;
		}
		return cnt;
	}

	void print (vector<int> &v){
		cout<<"[";
		for(int i=0; i<v.size() ; i++){
			cout<<v[i]<<" ";
		}
		cout<<"]";
		cout<<endl;
	}
	void getSubsets(vector<int> &count, int i, vector<int>& r){
		if(i == count.size()){
			//v.push_back(r);
			print(r);
			return;
		}
		int me = count[i];
		int mycount = count[i+1];
		i += 2;
		int j = 0;
		do{
			getSubsets(count, i, r);
			r.push_back(me);
			j++;
		}while(j <= mycount);
		for(j=0; j<=mycount; j++){
			r.pop_back();
		}
	}
	vector<vector<int> > subsetsWithDup(vector<int>& nums) {
	    sort(nums.begin(), nums.end());
	    vector<int> count;
	    for(int i = 0 ; i<nums.size();){
	    	count.push_back(nums[i]);
	    	count.push_back(countMe(nums[i], nums, i));
	    }
	    print(count);
	    vector<int> r;
	    getSubsets(count, 0, r);
	    return v;
	}
int main(){
	int N;
	cin>>N;
	vector<int> nums(N);
	for(int i=0; i<N; i++){
		cin>>nums[i];
	}
	subsetsWithDup(nums);
	return 0;

}