#include <bits/stdc++.h>

using namespace std;
#define it map<int, int>::iterator

/*it increment(it i, int c){
	while(c){
		i = next(i);
		c--;
	}
	return i;
}*/

int findLHS(vector<int>& nums) {
    map<int, int> cmap;
    for(int i=0; i<nums.size(); i++){
        cmap[nums[i]]++;
    }
    int mx = 0;
    it i = cmap.begin();
    it j = cmap.begin();
    if(cmap.size())
    	++j;
    for(; j != cmap.end(); ++i, ++j){
    	int prev = i->first;
        int curr = j->first;
        if(abs(curr - prev) == 1){
            mx = max(mx, i->second + j->second);
        }
    }
    return mx;
}
int main(){
	int size;
	cin>>size;
	vector<int> v(size);
	for(int i=0; i<size; i++){
		cin>>v[i];
	}
	cout<<findLHS(v)<<endl;
	return 0;
}