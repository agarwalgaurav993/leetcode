#include <bits/stdc++.h>

using namespace std;
	int getAR(vector<vector <int> > & ar, int i , int j){
		if(i>=0 && j>=0 && i< ar.size() && j<ar[i].size() && i<=j){
			return ar[i][j];
		}
		return 0;
	}
	int longestPalindromeSubseq(string s) {
		if(s.length() == 0)
			return 0;
		int N = s.length() +1;
		vector<vector <int> > ar(N);
		for(int i = 0; i < N; i++){
			ar[i].resize(N);
			ar[i][i] = 1;
		}
		ar[0][0] = 0;
		for(int len = 1; len < N - 1; len++){
			for(int i = 1; i < N-len; i++){
				int j = i + len;
				if(s[i-1] == s[j-1]){
					ar[i][j] = 2 + getAR(ar, i+1, j-1);
				}
				else{
					ar[i][j] = max(getAR(ar, i+1, j), getAR(ar,i, j-1));
				}
				//cout<<"ar [" <<i<<" "<<j<<"] : "<<ar[i][j]<<endl;
			}
		}
		return getAR(ar, 1, N - 1);

	}
int main()
{
	string s;
	cin>>s;
	cout<<longestPalindromeSubseq(s)<<endl;
	return 0;
}