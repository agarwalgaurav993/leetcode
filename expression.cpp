#include <bits/stdc++.h>
using namespace std;

	string exactNumber(int &b, string &s){
		int e = b;
		while(e < s.length() && s[e] >= '0' && s[e] <= '9')
			e++;
		string r = s.substr(b, e-b);
		b = e;
		return r; 
	}
	int myPriority(string c){
		if(c == "^")
			return 3;
		if(c == "*" || c == "/")
			return 2;
		if(c == "+" || c == "-")
			return 1;
		return -1;
	}
	vector<string> toPostfix(string &s){
		vector<string> postfix;
		vector<string> opstack;
		string op;
		for(int i=0; i<s.length(); ){
			if(s[i] == ' ')
				i++;
			else if(s[i] >= '0' && s[i] <= '9'){
				/*Extract Number*/
				postfix.push_back(exactNumber(i, s));
			}
			else{
				/*Action on opstack*/
				op = string(1,s[i++]);
				while(!opstack.empty() && myPriority(op) <= myPriority(opstack.back())){
					postfix.push_back(opstack.back());
					opstack.pop_back();
				}
				opstack.push_back(op);
			}

		}
		while(!opstack.empty()){
			postfix.push_back(opstack.back());
			opstack.pop_back();
		}
		return postfix;
	}

	int getTop(vector<int>& v){
		if(v.empty())
			return 0;
		int r = v.back();
		v.pop_back();
		return r;
	}
	int operate(int x, string op, int y){
		if(op == "+")
			return x + y;
		if(op == "-")
			return x - y;
		if(op == "*")
			return x * y;
		if(y == 0)
			return 0;
		return x/y;
	}
	int calculate(string &s) {
	    vector<string> postfix = toPostfix(s);
	 	vector<int> operandstk;
	 	int r;
	 	int x, y;
	 	for(int i=0; i<postfix.size(); i++){
	 		//cout<<postfix[i]<<endl;
	 		if(postfix[i][0] >= '0' && postfix[i][0] <= '9'){
	 			istringstream iss(postfix[i]);
	 			iss>>r;
	 			operandstk.push_back(r);
	 		}
	 		else{
	 			y = getTop(operandstk);
	 			x = getTop(operandstk);
	 			operandstk.push_back(operate(x, postfix[i], y));
	 		}
	 	}
	 	return operandstk.back();
	}
int main(){
	string s;
	cin>>s;
	cout<<calculate(s)<<endl;
	return 0;
}