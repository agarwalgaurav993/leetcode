#include <bits/stdc++.h>
using namespace std;

	int mx;
	void updateMax(int x){
		mx = max(mx, x);
	}
	int getLevel(string s, int&i){
		int level = 0;
		if(i<s.length() -1 && s[i] == '\\' && s[i+1] == 'n'){
			i+=2;
		}
		while(i<s.length()-1 && s[i] == '\\' && s[i+1] == 't'){
			level++;
			i+=2;
		}
		return level;
	}
	int getback(vector<int> &v){
		if(v.size())
			return v.back();
		return 0;
	}
	int getStringLength(string s, int& i){
		int len = i;
		while(i<s.length() && s[i] != '\\'){
			i++;

		}
		cout<<"Position of i : "<<i<<endl;
		cout<<"Length : "<<i-len<<endl;
		return i-len;
	}
	void getMaxLength(string s){
		int i =0;
		vector<int> ll;
		while(i < s.length()){
			int level = getLevel(s, i);
			if(level > ll.size()-1){
			/*Push*/
				ll.push_back(getback(ll) + getStringLength(s, i));
				int seperator = 0;
				if(ll.size() > 1)
					seperator = ll.size() - 1;
				updateMax(ll.back() + seperator);
			}
			else{
				while(ll.size()-1 > level-1){
					ll.pop_back();
				}
				ll.push_back(getback(ll) + getStringLength(s, i));
				int seperator = 0;
				if(ll.size() > 1)
					seperator = ll.size() - 1;
				updateMax(ll.back() + seperator);
			}
		}
	}
int main(){
	mx = 0;
	string s;
	cin>>s;
	getMaxLength(s);
	cout<<mx<<endl;
	return 0;
}