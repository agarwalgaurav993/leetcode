#include <bits/stdc++.h>
using namespace std;

	bool condition(int curr, int key){
		if(curr > key)
			return false;
		else
			return true;
	}
	int findPos(vector<int>& arr,int begin, int end, int key){
		/*T, T, T, T, T, F, F, F*/
		/*Find the last T*/
		if(begin == end){
			if(condition(arr[begin], key)){
				return begin;
			}
			else
				return begin - 1;
		}
		else if(begin < end){
			int mid = (begin + end) / 2;
			if(condition(arr[mid], key)){
				if(condition(arr[mid+1], key))
					return findPos(arr, mid+1, end, key);
				else
					return mid;
			}
			else{

				return findPos(arr, begin, mid -1, key);
			}
		}
		else if(begin > end){

			return findPos(arr, begin, begin, key);
		}
	}

	int findRadius(vector<int>& houses, vector<int>& heaters) {
		sort(heaters.begin(), heaters.end());
		int radius = INT_MIN;
		for(int i=0; i<houses.size(); i++){
			int pos = findPos(heaters, 0, heaters.size()-1, houses[i]);
			if(pos == -1){
				radius = max(abs(houses[i] - heaters[pos + 1]), radius);
			}
			else if(pos == heaters.size()-1){
				radius = max(abs(houses[i] - heaters[pos]), radius);
			}
			else{
				radius = max(min(abs(houses[i] - heaters[pos]), abs(houses[i] - heaters[pos+1])), radius);
			}
			//cout<<"Position of house "<< houses[i] <<" : "<< pos <<endl;
		}
		return radius;
	}
int main(){
	int n1, n2;
	cin>>n1>>n2;
	vector<int> heaters(n1);
	vector<int> houses(n2);
	for(int i=0; i<n1; i++){
		cin>>heaters[i];
	}
	for (int i = 0; i < n2; ++i)
	{
		/* code */
		cin>>houses[i];
	}
	cout<<findRadius(houses, heaters)<<endl;
	return 0;
}