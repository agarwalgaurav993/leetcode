#include <bits/stdc++.h>

using namespace std;

	//*Part - 1*/
	/*This Function would Check if Ith Index accounts for Yes/No as per the given target*/
	bool checkYesNoR(int i, int target, int c){
	    /*Currently This function would return True if ar[i] can be lower bound of target element*/
	    if(mat[i][c] <= target)
	        return true;
	    else
	        return false;
	}
	/*B-Search to Return Last Yes from the array of [Y, Y, Y, Y, Y, Y, N]*/
	/*Function returns index of the Last Yes in case Yes is present, else returns -1*/
	/*Note: Function to Select "m"*/
	int BSearchR(int s, int e, int c, int target){
	        if(s > e)
	            return -1;
	        if(s == e){
	            if(checkYesNoR(s, target, c))
	                return s;
	            else
	                return -1;
	        }
	        int m = s + (e - s + 1)/2;
	        if(checkYesNoR(m, target, c)){
	            s = m;
	            return  BSearchR(s, e, c, target);
	        }
	        else{
	            e = m - 1;
	            return BSearchR(s, e, c, target);
	        }
	}

	/*Part 2*/
	/*This Function would Check if Ith Index accounts for Yes/No as per the given target*/
	bool checkYesNoC(int i, int target, int r){
	    /*Currently This function would return True if ar[i] can be upper bound of target element*/
	    if(mat[r][i] >= target)
	        return true;
	    else
	        return false;
	}


	/*B-Search to Return First Yes from the array of [N, N, N, N, Y]*/
	/*Function returns index of the First Yes in case Yes is present, else returns -1*/
	/*Note: Function to Select "m"*/
	int BSearchC(int s, int e, int r, int target){
	        if(s > e)
	            return -1;
	        if(s == e){
	            if(checkYesNoC(s, target, r))
	                return s;
	            else
	                return -1;
	        }
	        int m = (s + e)/2;
	        if(checkYesNoC(m, target, r)){
	            e = m;
	            return BSearchC(s, e, r, target);
	        }
	        else{
	            s = m + 1;
	            return BSearchC(s, e, r, target);
	        }
	}
	bool searchMatrix(vector<vector<int> >& matrix, int target) {
		bool turn = true;
		int v = 0;
		int rows = matrix.size();
		int cols = matrix[0].size();
		int nv = 0;
		while(nv>=0 && v>=0){
			if(turn){
				nv = BSearchR(0, rows-1, v, target);
				if(nv>-1 && mat[nv][v] == target){
					return true;
				}
				turn = false;
				v = nv;
			}
			else{
				nv = BSearchC(0, cols-1, v, target);
				if(nv>-1 && mat[v][nv] == target){
					return true;
				}
				turn = true;
				v = nv;
			}
		}
		return false;
	}