#include <bits/stdc++.h>
using namespace std;
	int getAr(vector<vector<int> > &v, int i, int j){
		if(i <0 || j<0 || i >= v.size() || j >= v[i].size())
			return 0;
		return v[i][j];
	}
	int getAr1(vector<int> &v, int i){
		if(i<0 || i>= v.size())
			return 0;
		return v[i];
	}
	void print(vector<int> &v){
		for(int i=0; i<v.size(); i++)
			cout<<v[i]<<" ";
		cout<<endl;

	}
	int getMax(vector<int> &v){
		/*2 5 5 2 3 6 2*/
		print(v);
		int i=0;
		vector<int> cnt;
		vector<int> stk;
		int mx = 0;
		int c = 0;
		while(i<=v.size()){
			if(stk.empty() || stk.back() <= getAr1(v, i)){
				stk.push_back(getAr1(v, i));
				cnt.push_back(c + 1);
				i++;
				c = 0;
			}
			else{
				c += cnt.back();
				if(stk.back() <= c){
					mx = max(mx, stk.back()*stk.back());
					stk.clear();
					cnt.clear();
				}
				if(stk.size()){
					stk.pop_back();
					cnt.pop_back();
				}
				
			}
		}
		//cout<<mx<<endl; 
		return mx;
	}

	int maximalSquare(vector<vector<char> >& matrix) {
		int M = matrix.size();
		int N = matrix[0].size();
		int mx = 0;
		vector<vector<int> > v(M, vector<int>(N));
		for(int j=0; j<N; j++)
			for(int i=M-1; i>=0; i--){
				if(matrix[i][j] == '1'){
					v[i][j] = 1 + getAr(v, i+1, j);
				}
				else
					v[i][j] = 0;
			}
	    for(int i=0; i<M; i++){
	    	mx = max(mx, getMax(v[i]));
	    	
	    }
	    return mx;
	}

	int main(){
		int M, N;
		cin>>M>>N;
		vector<vector<char> > v(M, vector<char>(N));
		for(int i=0; i<M; i++){
			for(int j=0; j<N; j++){
				cin>>v[i][j];
			}
		}
		cout<<maximalSquare(v)<<endl;
		return 0;
	}