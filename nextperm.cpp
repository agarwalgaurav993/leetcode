#include <bits/stdc++.h>
using namespace std;

int getAr(vector<int> &nums, int i){
	if(i >= nums.size())
		return INT_MIN;
	return nums[i];
}
void nextPermutation(vector<int>& nums) {
	int top = nums.size();
	int i = top -1;
	while(i>-1 && (top == nums.size() || nums[i] >= nums[top])){
		i--;
		top--;
	}
	while(i!=-1 && top<nums.size() && nums[top] > nums[i])
		top++;

	if(i!=-1){
		top--;
		/*Exchange nums[i] & nums[top -1]*/
		nums[i] = nums[i] ^ nums[top];
		nums[top] = nums[i] ^ nums[top];
		nums[i] = nums[i] ^ nums[top];
		sort(nums.begin()+i+1, nums.end()); 
	}
}

int main(){
	int N;
	cin>>N;
	vector<int> gas(N);
	for(int i=0; i<N; i++)
		cin>>gas[i];
	nextPermutation(gas);
	for(int i=0; i<N; i++)
		cout<<gas[i]<<" ";
	cout<<endl;
	return 0;
}